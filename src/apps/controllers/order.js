const CategoryModel = require("../models/categories");
const ProductModel = require("../models/product");
const paginate = require("../../common/paginate");
const OrderModel = require("../models/order");
const index = async(req,res) => {
    const page = parseInt(req.query.page) || 1 ;// cau lenh if
    const limit = 10;
    // lấy ra document bị bỏ
    const skip = page*limit -limit;
    // lấy ra tổng số bản ghi
    const total = await OrderModel.find().countDocuments();
    // lấy ra số trang
    const totalPage = Math.ceil(total/limit);
    // lấy ra document
    const products = await OrderModel.find().skip(skip).limit(limit);
    res.render("admin/order/order" , {
        products:products,
        totalPage:totalPage,
        page:page,
        pages:paginate(page,totalPage),
    });
}
module.exports = {
    index
}