const UserModel = require("../models/user");
const paginate = require("../../common/paginate");
const add_user = async (req, res) => {
  let error = "";
  const name = req.body.user_full;
  const mail = req.body.user_mail;
  const pass = req.body.user_pass;
  const repass = req.body.user_re_pass;
  const roleNumber = req.body.user_level;
  var role;
  if (roleNumber == 1) {
    role = "admin";
  } else {
    role = "member";
  }
  const users = await UserModel.find({ email: mail });
  if (name == "" || mail == "" || pass == "" || repass == "") {
    error = "Bạn phải nhập đầy đủ thông tin!";
  } else if (pass != repass) {
    error = "Mật khẩu nhập lại cần phải khớp!";
  } else if (users.length > 0) {
    error = "Email đã tồn tại!";
  } else {
    const UserInsert = new UserModel({
      full_name: name,
      email: mail,
      password: pass,
      role: role,
    });
    await UserInsert.save();
  }

  res.redirect("/admin/users");
};

const create = (req, res) => {
  res.render("admin/user/add_user", { data: {} });
};
const index = async (req, res) => {
  const page = parseInt(req.query.page) || 1; // cau lenh if
  const limit = 10;
  // lấy ra document bị bỏ
  const skip = page * limit - limit;
  // lấy ra tổng số bản ghi
  const total = await UserModel.find().countDocuments();
  // lấy ra số trang
  const totalPage = Math.ceil(total / limit);
  // lấy ra document
  const users = await UserModel.find().skip(skip).limit(limit);
  res.render("admin/user/user", {
    users: users,
    totalPage: totalPage,
    page: page,
    pages: paginate(page, totalPage),
  });
};
const edit = async (req, res) => {
  const id = req.params.id;
  const user = await UserModel.findOne(id);
  res.render("admin/user/edit_user", { user });
};

const postEdit = async (req, res) => {
  const email = req.body.user_mail;
  const password = req.body.user_pass;
  const role = req.body.user_level;
  const full_name = req.body.user_full;
  const id = req.params.id;
  await UserModel.updateOne(
    { _id: id },
    {
      email,
      password,
      role,
      full_name,
    }
  );
  res.redirect("/admin/users");
};
const postDelete = async (req, res) => {
  const id = req.params.id;
  await UserModel.deleteOne({ _id: id });
  res.redirect("/admin/users");
};
module.exports = {
  create: create,
  index: index,
  edit: edit,
  add_user: add_user,
  postDelete: postDelete,
  postEdit: postEdit,
};
