const CategoryModel = require("../models/categories");
const ProductModel = require("../models/product");
const CommentModel = require("../models/comment");
const OrderModel = require("../models/order");
const paginate = require("../../common/paginate");
const sid = "ACfb67b8841ec97a96a5a81381cb5eb52f";
const authToken = "bc219a2ac9d9eb281e1b31bb6f518445";
const tiwlio = require("twilio")(sid, authToken);
const multer = require("multer");
const fs = require("fs");
const path = require("path");
const slug = require("slug");
const ejs = require("ejs");
const config = require("config");
const transporter = require("../../common/transporter");
const moment = require("moment");
const { STATUS } = require("../enum");
const home = async (req, res) => {
  const page = parseInt(req.query.page) || 1; // cau lenh if
  const limit = 9;
  // lấy ra document bị bỏ
  const skip = page * limit - limit;
  const total = await ProductModel.find({ is_stock: true }).countDocuments();
  const products = await ProductModel.find({ is_stock: true })
    .sort({ id: -1 })
    .skip(skip)
    .limit(limit);
  const totalPage = Math.ceil(total / limit);
  const FeaturedProducts = await ProductModel.find({
    is_stock: true,
    featured: true,
  }).limit(6);
  res.render("site/index", {
    FeaturedProducts: FeaturedProducts,
    products: products,
    totalPage: totalPage,
    page: page,
    pages: paginate(page, totalPage),
    total: total,
  });
};
const cart = async (req, res) => {
  const products = req.session.cart;
  res.render("site/cart", { products, totalPrice: 0 });
};

const updateCart = (req, res) => {
  const products = req.body.products;
  const items = req.session.cart;
  items.map((item) => {
    if (products[item.id]) {
      item.qty = parseInt(products[item.id]["qty"]);
    }
  });
  req.session.cart = items;
  res.redirect("/cart");
};

const product = async (req, res) => {
  const id = req.params.id;
  const product = await ProductModel.findById(id);
  const comments = await CommentModel.find({ prd_id: id });
  res.render("site/product", { product, comments });
};
const comment = async (req, res) => {
  const id = req.params.id;

  const comment = {
    prd_id: id,
    full_name: req.body.full_name,
    email: req.body.email,
    body: req.body.body,
  };
  await CommentModel(comment).save();
  res.redirect(req.path);
};
const category = async (req, res) => {
  const slug = req.params.slug;
  const id = req.params.id;
  const page = parseInt(req.query.page) || 1; // cau lenh if
  const limit = 9;
  // lấy ra document bị bỏ
  const skip = page * limit - limit;
  const total = await ProductModel.find({ cat_id: id }).countDocuments();
  const products = await ProductModel.find({ cat_id: id })
    .sort({ id: -1 })
    .skip(skip)
    .limit(limit);
  const totalPage = Math.ceil(total / limit);
  const category = await CategoryModel.findById(id);
  const title = category.title;
  res.render("site/category", {
    products: products,
    totalPage: totalPage,
    page: page,
    pages: paginate(page, totalPage),
    title: title,
    category: category,
    total: total,
  });
};
const search = async (req, res) => {
  const keyword = req.query.keyword || "";
  const filter = {};
  if (keyword) {
    filter.$text = { $search: keyword };
  }
  const page = parseInt(req.query.page) || 1; // cau lenh if
  const limit = 9;
  // lấy ra document bị bỏ
  const skip = page * limit - limit;
  const total = await ProductModel.find(filter).countDocuments();
  const products = await ProductModel.find(filter)
    .sort({ id: -1 })
    .skip(skip)
    .limit(limit);
  const totalPage = Math.ceil(total / limit);
  // console.log(products);
  res.render("site/search", {
    keyword,
    products: products,
    totalPage: totalPage,
    page: page,
    pages: paginate(page, totalPage),
    total: total,
  });
};
const addToCart = async (req, res) => {
  const body = req.body;
  let items = req.session.cart;
  let isUpdate = false;
  // mua lai san pham da mua roi
  items.map((item) => {
    if (items.id === body.id) {
      isUpdate = true;
      item.qty += parseInt(body.qty);
    }
    return item;
  });
  // mua 1 san pham moi
  if (!isUpdate) {
    const product = await ProductModel.findById(body.id);
    items.push({
      id: product.id,
      name: product.name,
      price: product.price,
      img: product.thumbnail,
      qty: parseInt(body.qty),
    });
  }
  req.session.cart = items;
  res.redirect("/cart");
};
const order = async (req, res) => {
  const items = req.session.cart;
  const body = req.body;
  // Lấy ra đường dẫn đến thư mục views
  const viewPath = req.app.get("views");
  // Compile template EJS sang HTML để gửi mail cho khách hàng
  const html = await ejs.renderFile(
    path.join(viewPath, "site/email-order.ejs"),
    {
      name: body.name,
      phone: body.phone,
      mail: body.mail,
      add: body.add,
      // url: config.get("app.url"),
      totalPrice: 0,
      items,
    }
  );
  // Gửi mail
  transporter
    .sendMail({
      to: body.mail,
      from: "BBBSHOP",
      subject: "Xác nhận đơn hàng từ BBBSHOP",
      html,
    })
    .then(async (data) => {
      const currentDateVN = moment().utcOffset(420).format("DD/MM/YYYY");
      const order = new OrderModel(
        {
          full_name: body.name,
          phone: body.phone,
          email: body.mail,
          address: body.add,
          product: items,
          time: currentDateVN,
          status : STATUS.CHOXACNHAN
        },
        { versionKey: false }
      );
      await order.save();
    })
    .catch((err) => console.log(err));
  const phoneNumber = body.phone.substr(1, body.phone.length);
  req.session.cart = [];
  tiwlio.messages
    .create({
      body: "Đơn hàng của quý khách đã hoàn thành chúng tôi sẽ liên hệ với quý khách trong thời gian sớm nhất. Cảm ơn quý khác đã tin tưởng và sử dụng dịch vụ của chúng tôi, chúc quý khách có 1 ngày mới thật là vui vẻ hạnh phúc và tràn đầy năng lượng",
      from: "+19294160149",
      to: `+84${phoneNumber}`,
    })
    .then((message) => console.log(message.sid));
  res.redirect("/success");
};
const success = (req, res) => {
  res.render("site/success");
};
const delCart = (req, res) => {
  const id = req.params.id;
  const items = req.session.cart;
  items.map((item, key) => {
    if (item.id === id) {
      items.splice(key, 1);
    }
    return item;
  });
  req.session.cart = items;
  res.redirect("/cart");
};

  const payment = (req , res)=> {
    res.render('site/payment')
  }
module.exports = {
  home: home,
  cart: cart,
  category: category,
  search: search,
  success: success,
  product: product,
  comment: comment,
  addToCart: addToCart,
  updateCart: updateCart,
  order: order,
  delCart: delCart,
  payment : payment
};
