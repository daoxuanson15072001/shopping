const CategoryModel = require("../models/categories");
const paginate = require("../../common/paginate");
const create = (req, res) => {
  res.render("admin/category/add_category");
};
const postCreate = async (req, res) => {
  const cat_name = req.body.cat_name;
  const CategoryInsert = new CategoryModel({
    title: cat_name,

    slug: cat_name,

    description: null,
  });
  console.log(CategoryInsert);
  await CategoryInsert.save();

  res.redirect("/admin/categories");
};
const category = async (req, res) => {
  const page = parseInt(req.query.page) || 1; // cau lenh if
  const limit = 10;
  // lấy ra document bị bỏ
  const skip = page * limit - limit;
  // lấy ra tổng số bản ghi
  const total = await CategoryModel.find().countDocuments();
  // lấy ra số trang
  const totalPage = Math.ceil(total / limit);
  // lấy ra document
  const categories = await CategoryModel.find().skip(skip).limit(limit);
  res.render("admin/category/category", {
    categories: categories,
    totalPage: totalPage,
    page: page,
    pages: paginate(page, totalPage),
  });
};
const edit = (req, res) => {
  res.render("admin/category/edit_category");
};
const postEdit = async (req, res) => {
  const cat_name = req.body.cat_name;
  const id = req.params.id;
  console.log({ cat_name, id });
  await CategoryModel.updateOne(
    { _id: id },
    {
      title: cat_name,

      slug: cat_name,

      description: null,
    }
  );
  res.redirect("/admin/categories");
};

const postDelete = async (req, res) => {
  const id = req.params.id;
  await CategoryModel.deleteOne({ _id: id });
  res.redirect("/admin/categories");
};
module.exports = {
  postCreate: postCreate,
  create: create,
  category: category,
  edit: edit,
  postEdit: postEdit,
  postDelete: postDelete
};
