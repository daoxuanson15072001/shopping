const STATUS = {
  DAGIAOHANG: "Đã giao hàng",
  DANGGIAOHANG: "Đang giao hàng",
  DAHUY: "Đã huỷ",
  CHOXACNHAN: "Chờ xác nhận",
};
module.exports = {
  STATUS,
};
