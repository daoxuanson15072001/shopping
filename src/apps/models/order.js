const mongoose = require("../../common/database")();
const orderSchema = new mongoose.Schema(
  {
    full_name: {
      type: String,
      default: null,
    },
    phone: {
      type: String,
      default: null,
    },
    email: {
      type: String,
      default: null,
    },
    address: {
      type: String,
      default: null,
    },
    product: {
      type: Object,
      default: null,
    },
    time: {
      type: String,
      default: null,
    },
    status : {
      type: String,
      enum: ['Đã giao hàng', 'Đang giao hàng' , 'Đã huỷ' , 'Chờ xác nhận'],
      default : 'Chờ xác nhận'
    }
  },
  {
    timestamps: true,
  }
);
const OrderModel = mongoose.model("Order", orderSchema, "orders");
module.exports = OrderModel;
