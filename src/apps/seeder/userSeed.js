const UserModel = require("../models/user");
const mongoose = require("mongoose");
const dev = require("../../../config/default"); //get your mongoose string
//create your array. i inserted only 1 object here
const user = [   
  new UserModel({
    full_name: "admin",
    email: "admin@gmail.com",
    password: "password",
    role: "admin"
  }),]
//save your data. this is an async operation
//after you make sure you seeded all the user, disconnect automatically
user.map(async (p, index) => {
  await p.save((err, result) => {
    if (index === user.length - 1) {
      console.log("DONE!");
      mongoose.disconnect();
    }
  });
});