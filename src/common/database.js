const mongoose = require("mongoose");
module.exports = () => {
  mongoose.connect('mongodb://localhost/shopping', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
});
    return mongoose;
}